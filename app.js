const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const config = require('./config/database.config');
const cors = require('cors');

// on se connecte à la base de données
mongoose.connect('mongodb://localhost:27017/Musique', { useNewUrlParser: true });
const app = express();

var db =mongoose.connection;
db.once('open',function(){
    console.log("connection à la base ok");

});

const indexRouter = require('./routes/index');


const artistsRouter = require('./routes/artists');
const artistRouter = require('./routes/artist');
const albumsRouter = require('./routes/albums');
const albumRouter = require('./routes/album');
const tracksRouter = require('./routes/tracks');
const trackRouter = require('./routes/track');

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.use('/artist', artistRouter);
app.use('/artists', artistsRouter);

app.use('/album', albumRouter);
app.use('/albums', albumsRouter);

app.use('/track', trackRouter);
app.use('/tracks', tracksRouter);

module.exports = app;

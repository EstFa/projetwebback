const mongoose = require('mongoose');

const albumSchema = new mongoose.Schema(
    {

        name: String,
        release: Date,
        genre: String,
        original: String

    }

);

module.exports = mongoose.model('Album', albumSchema);

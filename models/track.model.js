const mongoose = require('mongoose');

const trackSchema = new mongoose.Schema(
    {

        name: String,
        duration: Number,
        listenings:Number,
        likes:Number


    }

);

module.exports = mongoose.model('Track', trackSchema);

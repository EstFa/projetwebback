const Artist = require('../models/artist.model.js');
const _ = require('lodash');

// Create and Save a new Artist
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    // If ame is not present in body reject the request by
    // sending the appropriate http code
    return res.status(400).send({
      message: 'first name can not be empty'  
    });
  }

  // Create a new Artist
  const artist = new Artist({
    name: req.body.name,
    birth:req.body.birth,
    followers:req.body.followers,
    album:req.body.album

  });

  // Save Artist in the database
  artist
    .save()
    .then(data => {
      // we wait for insertion to be complete and we send the newly artist integrated
      res.send(data);
    })
    .catch(err => {
      // In case of error during insertion of a new artist in database we send an
      // appropriate message
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Artist.'
      });
    });
};

// Retrieve and return all Artists from the database.
exports.findAll = (req, res) => {
  Artist.find()
    .then(artists => {
      res.send(artists);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving artists.'
      });
    });
};

// Retrieve and return all Artists with followersfrom the database.
exports.findFollowers = (req, res) => {
  Artist.find()
    .then(artists => {
              const allFollowers = _.map(artists, function(artist){
                var result = _.pick(artist,['name','followers'])
                return result;
              })
      res.send(allFollowers);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving artists.'
      });
    });
};

// Retrieve and return all Artists with number of Album from the database.
exports.findNAlbum = (req, res) => {
  Artist.find()
    .then(artists => {
              const allAlbum = _.map(artists, function(artist){
                var result = _.pick(artist,['name','album'])
                return result;
              })
      res.send(allAlbum);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving artists.'
      });
    });
};

// Find a single Artist with a id
exports.findOne = (req, res) => {
  Artist.findById(req.params.id)
    .then(artist => {
      if (!artist) {
        return res.status(404).send({
          message: 'Artsit not found with id ' + req.params.id
        });
      }
      res.send(artist);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error retrieving Artist with id ' + req.params.id
      });
    });
};

// Update a Artist identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.name) {
    return res.status(400).send({
      message: 'first name can not be empty'
    });
  }

  // Find artist and update it with the request body
  Artist.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.name,
      
    }
  )
    .then(artist => {
      if (!artist) {
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params.id
        });
      }
      res.send(artist);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Arstist not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error updating artist with id ' + req.params.id
      });
    });
};

// Delete a Artsit with the specified id in the request
exports.delete = (req, res) => {
 Artist.findByIdAndRemove(req.params.id)
    .then(artist => {
      if (!artist) {
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params.id
        });
      }
      res.send({ message: 'Artist deleted successfully!' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Could not delete artist with id ' + req.params.id
      });
    });
};

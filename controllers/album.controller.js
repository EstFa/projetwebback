const Album = require('../models/album.model.js');
const _ = require('lodash');


// Create and Save a new Album
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    // If ame is not present in body reject the request by
    // sending the appropriate http code
    return res.status(400).send({
      message: 'first name can not be empty'
    });
  }

  // Create a new Album
  const album = new Album({
    name: req.body.name,
    release: req.body.release,
    genre: req.body.genre,
    original: req.body.original

  });

  // Save Album in the database
  album
    .save()
    .then(data => {
      // we wait for insertion to be complete and we send the newly album integrated
      res.send(data);
    })
    .catch(err => {
      // In case of error during insertion of a new album in database we send an
      // appropriate message
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Album.'
      });
    });
};

// Retrieve and return all Albums from the database.
exports.findAll = (req, res) => {
  Album.find()
    .then(albums => {
      res.send(albums);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving albums.'
      });
    });
};


// Retrieve and return all albums with genre from the database.
exports.findGenre = (req, res) => {
  Album.find()
    .then(albums => {
      const allGenre = _.map(albums, function (album) {
        var result = _.pick(album, ['name', 'genre'])
        return result;
      })
      res.send(allGenre);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving albums.'
      });
    });
};


// Retrieve and return all albums with genre from the database.
exports.findPic = (req, res) => {
  Album.find()
    .then(albums => {
      const allPic = _.map(albums, function (album) {
        var result = _.pick(album, ['original'])
        return result;
      })
      res.send(allPic);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving albums.'
      });
    });
};

// Find a single Album with a id
exports.findOne = (req, res) => {
  Album.findById(req.params.id)
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params.id
        });
      }
      res.send(album);

    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Album not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error retrieving Album with id ' + req.params.id
      });
    });
};

// Update a Album identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.name) {
    return res.status(400).send({
      message: 'first name can not be empty'
    });
  }

  // Find album and update it with the request body
  Album.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.name,

    }
  )
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Album not found with id ' + req.params.id
        });
      }
      res.send(album);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Arstist not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error updating album with id ' + req.params.id
      });
    });
};

// Delete a Artsit with the specified id in the request
exports.delete = (req, res) => {
  Album.findByIdAndRemove(req.params.id)
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Album not found with id ' + req.params.id
        });
      }
      res.send({ message: 'Album deleted successfully!' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Album not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Could not delete album with id ' + req.params.id
      });
    });
};

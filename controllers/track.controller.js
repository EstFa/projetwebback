const Track = require('../models/track.model.js');
const _ = require('lodash');


// Create and Save a new Track
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    // If ame is not present in body reject the request by
    // sending the appropriate http code
    return res.status(400).send({
      message: 'first name can not be empty'
    });
  }

  // Create a new Track
  const track = new Track({
    name: req.body.name,
    duration: req.body.duration,
    listenings: req.body.listenings,
    likes: req.body.likes
  });

  // Save Track in the database
  track
    .save()
    .then(data => {
      // we wait for insertion to be complete and we send the newly track integrated
      res.send(data);
    })
    .catch(err => {
      // In case of error during insertion of a new track in database we send an
      // appropriate message
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Track.'
      });
    });
};

// Retrieve and return all Tracks from the database.
exports.findAll = (req, res) => {
  Track.find()
    .then(tracks => {
      res.send(tracks);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving tracks.'
      });
    });
};

// Find a single Track with a id
exports.findOne = (req, res) => {
  Track.findById(req.params.id)
    .then(track => {
      if (!track) {
        return res.status(404).send({
          message: 'Artsit not found with id ' + req.params.id
        });
      }
      res.send(track);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Track not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error retrieving Track with id ' + req.params.id
      });
    });
};




// Retrieve and return all likes with names from the database.
exports.findLike = (req, res) => {
  Track.find()
    .then(tracks => {
      const allLikes = _.map(tracks, function (track) {
        var result = _.pick(track, ['name', 'likes'])
        return result;
      })
      res.send(allLikes);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving tracks.'
      });
    });
};


// Retrieve and return all tracks with listennings from the database.
exports.findListen = (req, res) => {
  Track.find()
    .then(tracks => {
      const allListen = _.map(tracks, function (track) {
        var result = _.pick(track, ['name', 'listenings'])
        return result;
      })
      res.send(allListen);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving tracks.'
      });
    });
};


// Update a Track identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.name) {
    return res.status(400).send({
      message: 'first name can not be empty'
    });
  }

  // Find track and update it with the request body
  Track.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.name,

    }
  )
    .then(track => {
      if (!track) {
        return res.status(404).send({
          message: 'track not found with id ' + req.params.id
        });
      }
      res.send(track);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Arstist not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Error updating track with id ' + req.params.id
      });
    });
};

// Delete a Artsit with the specified id in the request
exports.delete = (req, res) => {
  Track.findByIdAndRemove(req.params.id)
    .then(track => {
      if (!track) {
        return res.status(404).send({
          message: 'track not found with id ' + req.params.id
        });
      }
      res.send({ message: 'Track deleted successfully!' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Track not found with id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Could not delete track with id ' + req.params.id
      });
    });
};

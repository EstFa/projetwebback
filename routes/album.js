var express = require('express');
var router = express.Router();
// we import our albums controller
var album = require('../controllers/album.controller');




/* GET artists and their followers */
router.get('/genre', album.findGenre);



/* GET artists and their followers */
router.get('/cover', album.findPic);


/* GET one album */
//router.get('/:id', album.findOne);

/* DELETE  one album */
router.delete('/:id', album.delete);
/* update  one album */
router.post('/:id', album.update);

/* create  one album*/
router.put('/', album.create);

module.exports = router;

var express = require('express');
var router = express.Router();
var album = require('../controllers/album.controller');

/* GET albums listing. */
router.get('/', album.findAll);

module.exports = router;

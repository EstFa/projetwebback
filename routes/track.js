var express = require('express');
var router = express.Router();
// we import our tracks controller
var track = require('../controllers/track.controller');

/* GET one track */
router.get('/likes', track.findLike);

/* GET one track */
router.get('/listen', track.findListen);

/* DELETE  one track */
router.delete('/:id', track.delete);
/* update  one track*/
router.post('/:id', track.update);

/* create  one track*/
router.put('/', track.create);

module.exports = router;

var express = require('express');
var router = express.Router();
var track = require('../controllers/track.controller');

/* GET tracks listing. */
router.get('/', track.findAll);

module.exports = router;

var express = require('express');
var router = express.Router();
// we import our artists controller
var artist = require('../controllers/artist.controller');

/* GET artists and their followers */
router.get('/followers', artist.findFollowers);

/* GET artists and their albums */
router.get('/album', artist.findNAlbum);

/* GET one artist
router.get('/:id', artist.findOne); */



/* DELETE  one artist */
router.delete('/:id', artist.delete);
/* update  one artist */
router.post('/:id', artist.update);

/* create  one artist */
router.put('/', artist.create); 

module.exports = router;
